
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 22 01:36:38 2018

@author: devin
"""
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
import os
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)
#%% UPLOADS FITNESS DATA FOR LIG AND DETERMINES THE MUTATIONAL CONNECTIONS FOR EACH GENOTYPE
library='LIG14'
data_file = "../Pickle/normLIGunLIG.p".format(library) #loads data from a dictionary pickle of genotype : relative fitness
DATA_fit = pickle.load( open(data_file,'rb'))
def mutcalc2(seq,WT): #determines the number of mutations
    mut=0 #number of mutations from WT
    for i in range(1,len(WT)+1):
        if seq[i-1:i]!=WT[i-1:i]:
            mut+=1
            if mut>1:
                break
    return mut
if os.path.isfile('../Pickle/{}_mut_options.p'.format(library))==True:
    mut_options = pickle.load(open('../Pickle/{}_mut_options.p'.format(library),'rb'))
else:
    mut_options={} # builds a dictionary of all of the possible mutational options for each genotype
    for seq in DATA_fit:
        mut_options[seq]=[]
    for seq in DATA_fit:
        for seq2 in DATA_fit:
            if mutcalc2(seq,seq2)==1:
                mut_options[seq].append(seq2)
    pickle.dump(mut_options, open('../Pickle/{}_mut_options.p'.format(library),'wb'))

#%% IDENTIFIES THE PEAKS IN THE LIGASE LANDSCAPE
peak=[]
for seq in mut_options:
    x=[]
    for seq2 in mut_options[seq]:
        x.append(DATA_fit[seq2])
    y=max(x)
    if (y-DATA_fit[seq])<0:
        peak.append(seq)

#%% FUNCTION TO PLOT THE LOCAL FITNESS LANDSCAPE OF A GIVEN PEAK
def peakanalysis(peak):
    mut1=[]
    mut1=mut_options[peak]
    num=0
    mut123={}
    for x in mut1:
        for y in DATA_fit:
            if y in mut_options[x] and y!=peak:
                num+=1
                mut123[num]=[DATA_fit[peak],DATA_fit[x],DATA_fit[y]]
    sns.set(rc={"figure.figsize": (4, 4)})
    sns.set_style("whitegrid")
    sns.set_palette(sns.color_palette("PuBu", 49))
    for x in mut123:
        plt.plot([mut123[x][0],mut123[x][1],mut123[x][2]])
        plt.axhline(DATA_fit[peak], ls='--', c='black', lw=2)
    xtick=(0,1,2)
    plt.xlim(0,2)
    plt.yticks(weight='bold',fontsize=15)
    plt.xticks(xtick, weight='bold',fontsize=15)
    plt.xlabel('Mutations',weight='bold',fontsize=15)
    plt.ylabel('Relative Ligase Fitness',weight='bold',fontsize=15)
    plt.title('{}'.format(peak),weight='bold',fontsize=15)
    return mut123
#%% PLOTS THE LOCAL FITNESS LANDSCAPE OF ALL PEAKS IN THE LIGASE LANDSCAPE
n=1
for seq in peak:
    plt.figure(n)
    peakanalysis(seq)
    n+=1

#%% UPLOADS FITNESS DATA FOR HDV AND DETERMINES THE MUTATIONAL CONNECTIONS FOR EACH GENOTYPE
library='HDV14'
data_file = "../Pickle/normHDVtotal.p".format(library) #loads data from a dictionary pickle of genotype : relative fitness
DATA_fit = pickle.load( open(data_file,'rb'))
def mutcalc2(seq,WT): #determines the number of mutations
    mut=0 #number of mutations from WT
    for i in range(1,len(WT)+1):
        if seq[i-1:i]!=WT[i-1:i]:
            mut+=1
            if mut>1:
                break
    return mut
if os.path.isfile('../Pickle/{}_mut_options.p'.format(library))==True:
    mut_options = pickle.load(open('../Pickle/{}_mut_options.p'.format(library),'rb'))
else:
    mut_options={} # builds a dictionary of all of the possible mutational options for each genotype
    for seq in DATA_fit:
        mut_options[seq]=[]
    for seq in DATA_fit:
        for seq2 in DATA_fit:
            if mutcalc2(seq,seq2)==1:
                mut_options[seq].append(seq2)
    pickle.dump(mut_options, open('../Pickle/{}_mut_options.p'.format(library),'wb'))

#%% IDENTIFIES THE PEAKS IN THE HDV LANDSCAPE
peak=[]
for seq in mut_options:
    x=[]
    for seq2 in mut_options[seq]:
        x.append(DATA_fit[seq2])
    y=max(x)
    if (y-DATA_fit[seq])<0:
        peak.append(seq)

#%% FUNCTION TO PLOT THE LOCAL FITNESS LANDSCAPE OF A GIVEN PEAK
def peakanalysis(peak):
    mut1=[]
    mut1=mut_options[peak]
    num=0
    mut123={}
    for x in mut1:
        for y in DATA_fit:
            if y in mut_options[x] and y!=peak:
                num+=1
                mut123[num]=[DATA_fit[peak],DATA_fit[x],DATA_fit[y]]
    sns.set(rc={"figure.figsize": (4, 4)})
    sns.set_style("whitegrid")
    sns.set_palette(sns.color_palette("YlOrRd", 49))
    for x in mut123:
        plt.plot([mut123[x][0],mut123[x][1],mut123[x][2]])
        plt.axhline(DATA_fit[peak], ls='--', c='black', lw=2)
    xtick=(0,1,2)
    plt.xlim(0,2)
    plt.yticks(weight='bold',fontsize=15)
    plt.xticks(xtick, weight='bold',fontsize=15)
    plt.xlabel('Mutations',weight='bold',fontsize=15)
    plt.ylabel('Relative HDV Fitness',weight='bold',fontsize=15)
    plt.title('{}'.format(peak),weight='bold',fontsize=15)
    return mut123
#%% PLOTS THE LOCAL FITNESS LANDSCAPE OF ALL PEAKS IN THE LIGASE LANDSCAPE (LIMITED TO THE FIRST TEN RIGHT NOW)
n=1
for seq in peak:
    if n<10:
        plt.figure(n)
        peakanalysis(seq)
        n+=1
