#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 09:30:44 2019

@author: devin
"""
import pickle
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pandas as pd
from scipy import stats
font = {'family' : 'sans-serif','weight' : 'bold','size'   : 15}
plt.rc('font', **font)

#%% LOADS DATA FOR LIGASE HTS AND qPCR CORRELATION
LIG=pickle.load(open('../Pickle/normLIGunLIG.p','rb'))
qPCR={'AAGCGGCTCTAGCG':[0.000359,0.000538],
'AATGGGGTTTACAG':[0.001565,0.001501],
'AATGCGCCCCAGAG':[0.000012,0.000008],
'CAGCGTCTCCGGCC':[0.001186,0.000311]}

LIG_delta={'AAGCGGCTCTAGCG':0.0016,
'AATGGGGTTTACAG':0.0007,
'AATGCGCCCCAGAG':0.0017,
'CAGCGTCTCCGGCC':0.0002}

qPCR_delta={'AAGCGGCTCTAGCG':8.949999999999998e-05,
'AATGGGGTTTACAG':3.200000000000002e-05,
'AATGCGCCCCAGAG':2.0000000000000003e-06,
'CAGCGTCTCCGGCC':0.0004375}

#%% PLOT qPCR MEASUREMENTS OF FOUR LOW FITNESS INTERSECTION SEQUENCES (FIG S5C)
plt.figure(1,figsize=[4,4])
sns.set_palette('Blues_r',8)
means=[]
for seq in qPCR:
    means.append(np.mean(qPCR[seq]))
plt.bar([1,2,3,4],means,yerr=list(qPCR_delta.values()),edgecolor='black',linewidth=2,capsize=3,error_kw=dict(lw=2, capsize=4, capthick=2))
plt.yticks([0,0.0005,0.0010,0.0015],[0.0,0.05,0.1,0.15],weight='bold',fontsize=12)
plt.xticks([1,2,3,4],weight='bold',fontsize=12)
plt.xlabel('intersection sequence',weight='bold',fontsize=12)
plt.ylabel('qPCR (% ligated)',weight='bold',fontsize=12)
sns.despine(trim=True)
plt.savefig('../Figures/LIG_qPCR_barplot.png',bbox_inches='tight',dpi=1000,transparent=True)


#%% PLOT CORRELATION BETWEEN qPCR AND SEQUENCING DATA FOR LIGASE (FIG S5B)
plt.figure(2,figsize=[4,4])
seq=[3.748462479,8.637172381,9.842328199,11.24310487,0.081332645,3.748462479,0.470781289,3.705986104,0.87105622,0.016533124,14.07135721,0.133877029,16.16284657,0.588724732,16.16284657,0.023642153,0.117101631,0.054255498,0.2651077838]
seq_error=[0.62,0.39,0.933,1.1,0.0042,0.62,0.045,0.41,0.025,0.001,1.7,0.01,0.31,0.084,0.31,0.002,0.0040,0.007,0.02]
q_error=[0.0,0.,0.,0,0,0,0.,0.,0.,0.,0,0.,0.,0.,0.,0.,0.,0.,0.]
qPCR=[0.11224284,0.306916134,0.230414317,0.191142016,0.000317874,0.109733126,0.010156808,0.241437286,0.051841201,4.51E-05,0.221518236,0.000321615,0.26468546,0.023115645,0.236351526,-0.000344747,0.000284759,0.002895444,0.000103159]

color='blue'
datas=pd.DataFrame({'x':qPCR,'y':seq})
g=sns.JointGrid(x='x',y='y',data=datas)
g = g.plot_joint(plt.scatter, color=color,edgecolor="black",s=100)
plt.errorbar(qPCR,seq,xerr=q_error,yerr=seq_error,color='blue',ecolor='black',zorder=0,fmt='o')
g = g.plot_joint(sns.regplot, color=color)
plt.ylim(0,19)
plt.xlim(0,0.35)
plt.ylabel('High-Throughput Sequencing',weight='bold',fontsize=15)
plt.xlabel('qPCR',weight='bold',fontsize=15)
slope, intercept, r_value, p_value, std_err = stats.linregress(seq, qPCR)
rho, pval = stats.spearmanr(seq,qPCR)
print('Spear :', rho, pval)
print ('R^2 : ', r_value**2,p_value)
plt.savefig('../Figures/LIG14_qPCR_correlation.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% PLOT CORRELATION BETWEEN P.A.G.E AND SEQUENCING DATA FOR HDV (FIG S5A)
seq= {'INT1':0.023681442,'INT2':0.03502397,'INT3':0.014705709,'INT4':0.055954572,'INT5':0.112549377,'INT6':0.677339641,'HDV7':1.25,'HDV_1.3':1.504755586,'HDV_anchor':0.998436692,'HDV_1.1':1.250161124,'HDV_0.9':0.973588093,'HDV_0.2':0.257528259,'HDV_0.4':0.362251542}
PAGE={'INT1':0.143423432,'INT2':0.151232134,'INT3':0.169842061,'INT4':0.175410207,'INT5':0.171190386,'INT6':0.88234234,'HDV7':1.25,'HDV_1.3':1.242342354,'HDV_anchor':1.132432342,'HDV_1.1':1.423234322,'HDV_0.9':1.22392068,'HDV_0.2':0.194424067,'HDV_0.4':0.4323111}
x_SE={'INT1':0.001797041,'INT2':0.000866241,'INT3':0.001822786,'INT4':0.003953057,'INT5':0.022800283,'INT6':0.02489122,'HDV7':0,'HDV_1.3':0.138251494,'HDV_anchor':0.031413356,'HDV_1.1':0.14603589,'HDV_0.9':0.074703558,'HDV_0.2':0.053193286,'HDV_0.4':0.046362965}
y_SE={'INT1':0.002642869,'INT2':0.002822806,'INT3':0.009209805,'INT4':0.010532965,'INT5':0.003010485,'INT6':0.020912726,'HDV7':0.015831869,'HDV_1.3':0.063557145,'HDV_anchor':0.042866534,'HDV_1.1':0.132158428,'HDV_0.9':0.035174852,'HDV_0.2':0.183626237,'HDV_0.4':0.24381928}


sns.set_palette('Reds_r',12)
def metric_correlate(lib1,lib2,xSE,ySE):
    plt.figure(3,figsize=[4,4])
    x=[]
    y=[]
    x_SE=[]
    y_SE=[]
    for genotype in lib1:
        x.append(lib1[genotype])
        y.append(lib2.get(genotype,0))
        x_SE.append(xSE[genotype])
        y_SE.append(ySE[genotype])
    datas=pd.DataFrame({'lib1':x,'lib2':y})
    g=sns.JointGrid(x='lib1',y='lib2',data=datas)

    g = g.plot_joint(sns.regplot, color='r')
    
    g = g.plot_joint(plt.scatter, color='r',edgecolor="black",s=100,linewidth=1)
    plt.errorbar(x,y,xerr=x_SE,yerr=y_SE,color='red',ecolor='black',zorder=0,fmt='o')
    #g = g.plot_marginals(sns.distplot, color="b")

    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    rho, pval = stats.spearmanr(x,y)
    plt.xticks([0.0,0.4,0.8,1.2,1.6],weight='bold',fontsize=15,color='black')
    plt.yticks([0.0,0.4,0.8,1.2,1.6],weight='bold',fontsize=15,color='black')
    plt.ylabel('High-Throughput Sequencing',weight='bold',fontsize=15,color='black')
    plt.xlabel('P.A.G.E.',weight='bold',fontsize=15,color='black')
    plt.xlim(-.1,1.65)
    plt.ylim(-.1,1.65)
    plt.savefig('../Figures/HDV14_seq-page.png',bbox_inches='tight',dpi=1000,transparent=True) #saves figure
    print ('R^2 : ', r_value**2)
    print('p-value :',p_value)
    print('rho :',rho)

metric_correlate(PAGE,seq,y_SE,x_SE)