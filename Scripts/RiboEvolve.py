#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 12 09:36:31 2018

@author: devin
"""

# =============================================================================
# Master code to develop a fitness landscape and simulate evolution using a 
# Wright-Fisher model - adapted from Bjorn Ostman matlab code
# - requires 7 arguments
#   1)library 2)population size 3)mutation rate 4)number of generations
#   5)starting genotype 6)number of replicates 7)results directory
# =============================================================================

#%% IMPORT NEEDED MODULES
import pickle
from random import random, randint
import numpy as np
import os
import sys

#%% FITNESS LANDSCAPE SETTINGS
print('Loading fitness data...')
WT='CGTCGTCCCCGGAC'
library = sys.argv[1] #'LIG14'
data_file = "../DATA/{}.p".format(library) #loads data from a dictionary pickle of genotype : relative fitness
DATA_fit = pickle.load( open(data_file,'rb'))
print('Fitness data         : ',data_file)
print('Library              : ',library)
print('Wildtype             : ',WT)
print('Number of genotypes  : ',len(DATA_fit))
print('Number of loci       : ',len(WT))
print('Fitness range        : ',min(DATA_fit.values()),'-',max(DATA_fit.values()))

#%% CALCULATE MUT DISTANCE FROM WT
print('Calculating mutational distance from wildtype...')
def mutcalc(seq,WT): #determines the number of mutations
    mut=0 #number of mutations from WT
    for i in range(1,len(WT)+1):
        if seq[i-1:i]!=WT[i-1:i]:
            mut+=1
    return mut

mutdist={}
for seq in DATA_fit:
    mutdist[seq]=mutcalc(seq,WT)

#%% GENERATE MUTATION OPTIONS
print('Calculating mutational options... (this could take a while)')
def mutcalc2(seq,WT): #determines the number of mutations
    mut=0 #number of mutations from WT
    for i in range(1,len(WT)+1):
        if seq[i-1:i]!=WT[i-1:i]:
            mut+=1
            if mut>1:
                break
    return mut
if os.path.isfile('../Pickle/{}_mut_options.p'.format(library))==True:
    mut_options = pickle.load(open('../Pickle/{}_mut_options.p'.format(library),'rb'))
else:
    mut_options={} # builds a dictionary of all of the possible mutational options for each genotype
    for seq in DATA_fit:
        mut_options[seq]=[]
    for seq in DATA_fit:
        for seq2 in DATA_fit:
            if mutcalc2(seq,seq2)==1:
                mut_options[seq].append(seq2)
    pickle.dump(mut_options, open('../Pickle/{}_mut_options.p'.format(library),'wb'))

#%% SETTINGS FOR EVOLUTION SIMULATION
print('Loading settings for evolution simulation...')
popsize = int(sys.argv[2]) #100
mu = float(sys.argv[3]) #0.01
gen = int(sys.argv[4]) #10
start_genotype = sys.argv[5] #'CGTCGTCCCCGGAC'
replicates = int(sys.argv[6]) #1
results_dir = sys.argv[7] #'HDV-LIG14'

max_fit=max(DATA_fit.values())
num_offspring = popsize
DATA_sims={'final_fitness':[],'final_diversity':[],'unique_sequences_explored':[],'mut_ben_total':[],'mut_del_total':[],'mut_nut_total':[],'sub_ben_total':[],'sub_del_total':[],'sub_nut_total':[]}
gen_fit_plot={}

#%% EVOLVE ON FITNESS LANDSCAPE
print('Evolving on landscape...')
for i in range(1,replicates+1):
    a_pop = []
    substitutions={0:start_genotype}
    diversity_explored=[]
    mut_ben=0
    mut_del=0
    mut_nut=0
    sub_ben=0
    sub_del=0
    sub_nut=0
    gen_fit_plot[i]=[]
    for j in range(popsize): #generates the starting population full of the starting genotype
        a_pop.append(start_genotype)
    for k in (range(1,gen+1)): 
        offspring=0
        a_offspring=[]
        while offspring < num_offspring: # runs through this loop until  the next generation is complete
            parent = a_pop[randint(0,popsize-1)] # randomly chooses a a member of the parent population
            if random() <= (DATA_fit[parent]/max_fit): # determines if the parent is 'fit' enough to reproduce
                offspring+=1
                if random() <= mu: # determines if the offspring will have a mutation
                    offspring_genotype=mut_options[parent][randint(0,len(mut_options[parent])-1)] #randomly chooses one of the mut_options
                    a_offspring.append(offspring_genotype)
                    if (DATA_fit[parent]-DATA_fit[offspring_genotype]) <= (1/gen): #determines if the mutation is beneficial
                        mut_ben += 1
                    elif (DATA_fit[parent]-DATA_fit[offspring_genotype]) >= (1/gen): #determines if the mutation is deleterious
                        mut_del += 1
                    else: #concludes that the mutation is neutral
                        mut_nut += 1
                else:
                    a_offspring.append(parent) #if offspring is not mutated, copys the parent genotype into offspring
        a_pop=a_offspring #makes the offspring population the parent population for the next generation
        x = np.unique(a_offspring) # determines the number of unique genotypes in the new population
        for seq in x:
            if seq not in diversity_explored: # adds each unique sequence to the diversity explored for this replicate
                diversity_explored.append(seq)
            if seq not in substitutions.values() and a_offspring.count(seq)>=(0.9*num_offspring): #determines if there is a new substitution mutation that is 90% of the population
                substitutions[len(substitutions)]=seq
        a_offspring_fit=[]
        for seq in a_offspring: # creates a list of the offspring fitnesses
            a_offspring_fit.append(DATA_fit[seq])
        gen_fit=np.mean(a_offspring_fit) #determines the average population fitness
        gen_fit_plot[i].append(gen_fit)
    DATA_sims['final_fitness'].append(gen_fit)
    DATA_sims['final_diversity'].append(len(np.unique(a_offspring)))
    DATA_sims['unique_sequences_explored'].append(len(diversity_explored))
    DATA_sims['mut_ben_total'].append(mut_ben)
    DATA_sims['mut_del_total'].append(mut_del)
    DATA_sims['mut_nut_total'].append(mut_nut)
    for seq in substitutions:
        if seq != 0:
            if DATA_fit[substitutions[seq-1]]-DATA_fit[substitutions[seq]] <= (1/gen): #determines if the substitution is beneficial
                sub_ben += 1
            elif DATA_fit[substitutions[seq-1]]-DATA_fit[substitutions[seq]] >= (1/gen): #determines if the substitution is deleterious
                sub_del += 1
            else:#concludes that the substituion is neutral
                sub_nut +=1 
    DATA_sims['sub_ben_total'].append(sub_ben)
    DATA_sims['sub_del_total'].append(sub_del)
    DATA_sims['sub_nut_total'].append(sub_nut)
    if ((i)%10==0):
        print ('Replicates : ',i)

#%% SAVE DATA INTO PICKLES
pickle.dump(DATA_sims, open('../Results/{}/DATA_sims_L-{}_P-{}_M-{}_G-{}_S-{}.p'.format(results_dir,library,popsize,mu,gen,start_genotype),'wb'))
pickle.dump(gen_fit_plot, open('../Results/{}/gen_fit_plot_L-{}_P-{}_M-{}_G-{}_S-{}.p'.format(results_dir,library,popsize,mu,gen,start_genotype),'wb'))

