# -*- coding: utf-8 -*-
"""

Created on Tue Jan 03 01:20:23 2017

@author: devinbendixsen
"""
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import numpy as np
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)
LIG=pickle.load(open("../Pickle/normLIGunLIG.p","rb"))
HDV=pickle.load(open("../Pickle/normHDVtotal.p", "rb"))
mutnum=pickle.load(open("../Pickle/mutnumHDVLIGLib14.p", "rb"))
edges=pickle.load(open("../Pickle/1wayconnections.p","rb"))
import matplotlib as mpl
mpl.rcParams['agg.path.chunksize'] = 10000
#%%
HDVmutmean={}
HDVmeanx=[]
HDVmeany=[]
for num in range(0,15):
    HDVmutmean[num]=[]
    HDVmeanx.append(num)
for seq in mutnum:
    HDVmutmean[mutnum[seq]].append(HDV[seq])
for num in HDVmutmean:
    HDVmeany.append(np.mean(HDVmutmean[num]))
LIGmutmean={}
LIGmeanx=[]
LIGmeany=[]
for num in range(0,15):
    LIGmutmean[num]=[]
    LIGmeanx.append(num)
for seq in mutnum:
    LIGmutmean[mutnum[seq]].append(LIG[seq])
for num in LIGmutmean:
    LIGmeany.append(np.mean(LIGmutmean[num]))
#%%
plt.figure(figsize=[12,8])
plt.subplot(211)
x=[]
y=[]
for seq in HDV:
    x.append(mutnum[seq])
    y.append(HDV[seq])
sns.violinplot(x,y,color='gainsboro',scale='width',cut=0,bw=0.2,inner=None)
plt.scatter(x,y,s=20,color='red',edgecolor='black')
x=[]
y=[]
for seq in edges:
    x.append(mutnum[edges[seq][0]])
    y.append(HDV[edges[seq][0]])
    x.append(mutnum[edges[seq][1]])
    y.append(HDV[edges[seq][1]])
    x.append(None)
    y.append(None)
plt.plot(x,y,color='lightcoral',zorder=0,alpha=0.7)
plt.plot(HDVmeanx,HDVmeany,linestyle='dashed',color='black',linewidth=2)
plt.yticks([0,0.5,1,1.5])
plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14],[])
plt.ylabel('HDV fitness',weight='bold',fontsize=15,color='black')
plt.subplot(212)
x=[]
y=[]
for seq in LIG:
    x.append(mutnum[seq])
    y.append(LIG[seq])
sns.violinplot(x,y,color='gainsboro',scale='width',cut=0,bw=0.2,inner=None)
plt.scatter(x,y,s=20,color='blue',edgecolor='black')
x=[]
y=[]
for seq in edges:
    x.append(mutnum[edges[seq][0]])
    y.append(LIG[edges[seq][0]])
    x.append(mutnum[edges[seq][1]])
    y.append(LIG[edges[seq][1]])
    x.append(None)
    y.append(None)
plt.plot(x,y,color='lightsteelblue',zorder=0,alpha=1)
plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14],['HDV',1,2,3,4,5,6,7,8,9,10,11,12,13,'Ligase'])
plt.plot(LIGmeanx,LIGmeany,linestyle='dashed',color='black',linewidth=2)
plt.ylabel('Ligase fitness',weight='bold',fontsize=15,color='black')
plt.xlabel('mutational distance',weight='bold',fontsize=15,color='black')
sns.despine(trim=True)
plt.savefig('../Figures/HDV-LIG_landscapes.png',bbox_inches='tight',dpi=1000,transparent=True)

