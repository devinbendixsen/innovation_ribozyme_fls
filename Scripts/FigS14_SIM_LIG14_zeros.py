#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 12:23:41 2018

@author: devin
"""

import matplotlib.pyplot as plt
import pickle
import seaborn as sns

#%%
DATA={'norm':pickle.load( open('../Results/HDV-LIG14/DATA_sims_L-LIG14_P-1000_M-0.01_G-1000_S-CATCGTGCCTGGAC.p','rb')),
      'zero':pickle.load( open('../Results/HDV-LIG14/DATA_sims_L-LIG14_P-1000_M-0.01_G-1000_S-CATCGTGCCTGGAC_zeros.p','rb'))}

final_fitness={}
for genotype in DATA:
    final_fitness[genotype]=DATA[genotype]['final_fitness']

#%%
plt.figure(1,figsize=(4,3))
x=[]
y=[]
replicates=100
for genotype in final_fitness:
    for i in range(replicates):
        x.append(genotype)
    z=final_fitness[genotype]
    for num in z:
        y.append(num)
colors={'norm':'steelblue','zero':'darkgrey'}
ax=sns.violinplot(x,y,scale='width',palette=colors,inner='sticks',bw=0.25,linewidth=1,cut=0.25)
plt.xticks([0,1],['0','0.005'],weight='bold',fontsize=15,color='black')
plt.yticks(weight='bold',fontsize=15,color='black')
plt.ylabel('final population fitness',weight='bold',fontsize=15,color='black')
plt.xlabel('fitness detection limit',weight='bold',fontsize=15,color='black')
sns.despine(trim=True)
plt.savefig('../Figures/LIG14_zeros.png',bbox_inches='tight',dpi=1000,transparent=True)