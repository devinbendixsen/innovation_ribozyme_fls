#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 09:40:38 2019

@author: devin
"""
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from scipy import stats
font = {'family' : 'sans-serif','weight' : 'bold','size'   : 15}
plt.rc('font', **font)
mut_options=pickle.load(open("../Pickle/HDV14_mut_options.p","rb"))
HDV=pickle.load(open("../Pickle/normHDVtotal.p","rb"))
LIG=pickle.load(open("../Pickle/normLIGunLIG.p","rb"))
mutnum=pickle.load(open("../Pickle/mutnumHDVLIGLib14.p","rb"))
#%%
def HDV_degree_calc(cutoff):
    degree={}
    for seq in mut_options:
        n=0
        for seq2 in mut_options[seq]:
            if HDV[seq2]>cutoff:
                n+=1
        degree[seq]=n
    return degree
def LIG_degree_calc(cutoff):
    degree={}
    for seq in mut_options:
        n=0
        for seq2 in mut_options[seq]:
            if LIG[seq2]>cutoff:
                n+=1
        degree[seq]=n
    return degree
def mutcalc(seq,WT): #determines the number of mutations
    mut=0 #number of mutations from WT
    for i in range(1,len(WT)+1):
        if seq[i-1:i]!=WT[i-1:i]:
            mut+=1
    return mut
#%%
def HDV_dist_calc(cutoff):
    dist={}
    HDV_cutoff={}
    for seq in HDV:
        if HDV[seq]>=cutoff:
            HDV_cutoff[seq]=HDV[seq]
    LIG_cutoff={}
    for seq in LIG:
        if LIG[seq]>=cutoff:
            LIG_cutoff[seq]=HDV[seq]
    for seq in HDV_cutoff:
        seq_dist=[]
        for seq2 in LIG_cutoff:
            seq_dist.append(mutcalc(seq,seq2))
        dist[seq]=min(seq_dist)
    return dist
def LIG_dist_calc(cutoff):
    dist={}
    HDV_cutoff={}
    for seq in HDV:
        if HDV[seq]>=cutoff:
            HDV_cutoff[seq]=HDV[seq]
    LIG_cutoff={}
    for seq in LIG:
        if LIG[seq]>=cutoff:
            LIG_cutoff[seq]=HDV[seq]
    for seq in LIG_cutoff:
        seq_dist=[]
        for seq2 in HDV_cutoff:
            seq_dist.append(mutcalc(seq,seq2))
        dist[seq]=min(seq_dist)
    return dist
#%%
cutoff_list=[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3]
HDV_mutdist={}
LIG_mutdist={}
HDV_degree={}
LIG_degree={}
for num in cutoff_list:
    HDV_mutdist[num]=HDV_dist_calc(num)
    LIG_mutdist[num]=LIG_dist_calc(num)
    HDV_degree[num]=HDV_degree_calc(num)
    LIG_degree[num]=LIG_degree_calc(num)

#%% PLOTS FIGS3C
sns.set_style(style="ticks")
plt.figure(1)
plt.figure(figsize=[4,4])
x=[]
y=[]
for num in HDV_mutdist:
    for seq in HDV_mutdist[num]:
        x.append(HDV_mutdist[num][seq])
        y.append(HDV_degree[num][seq])

color='red'
datas=pd.DataFrame({'x':x,'y':y})
g=sns.JointGrid(x='x',y='y',data=datas)
g = g.plot_joint(plt.scatter, color=color,edgecolor="white")
g = g.plot_marginals(sns.distplot, color=color,bins=20)
g = g.plot_joint(sns.regplot, color=color)
plt.xticks([0,2,4,6,8,10])
plt.ylim(-0.5,12.5)
plt.xlim(-0.5,10.5)
plt.xlabel('mutational distance',weight='bold',fontsize=15)
plt.ylabel('active connections',weight='bold',fontsize=15)
slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
rho, pval = stats.spearmanr(x,y)
print('Spear :', rho, pval)
print ('R^2 : ', r_value**2,p_value)
plt.savefig('../Figures/HDV14_dist_degree_correlation.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% PLOTS FIGS3D
plt.figure(2)
plt.figure(figsize=[4,4])
x=[]
y=[]
for num in LIG_mutdist:
    for seq in LIG_mutdist[num]:
        x.append(LIG_mutdist[num][seq])
        y.append(LIG_degree[num][seq])

color='blue'
datas=pd.DataFrame({'x':x,'y':y})
g=sns.JointGrid(x='x',y='y',data=datas)
g = g.plot_joint(plt.scatter, color=color,edgecolor="white")
g = g.plot_marginals(sns.distplot, color=color,bins=20)
g = g.plot_joint(sns.regplot, color=color)
plt.xticks()
plt.ylim(-0.5,12.5)
plt.xlim(-0.5,10.5)
plt.xticks([0,2,4,6,8,10])
plt.xlabel('mutational distance',weight='bold',fontsize=15)
plt.ylabel('active connections',weight='bold',fontsize=15)
slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
rho, pval = stats.spearmanr(x,y)
print('Spear :', rho, pval)
print ('R^2 : ', r_value**2,p_value)
plt.savefig('../Figures/LIG14_dist_degree_correlation.png',bbox_inches='tight',dpi=1000,transparent=True)
