#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 13:11:36 2019

@author: devin
"""
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy import stats
font = {'family' : 'sans-serif','weight' : 'bold','size' : 15}
plt.rc('font', **font)
sns.set_style('ticks')

#%% LOADS DATA
LIG=pickle.load(open("../Pickle/LIGLib14_DATA_unexpected.p","rb"))
HDV=pickle.load(open("../Pickle/HDVLib14_DATA_unexpected.p","rb"))
mutnum=pickle.load(open("../Pickle/mutnumHDVLIGLib14.p", "rb"))
HDVtotal=pickle.load(open("../Pickle/normHDVtotal.p","rb"))
HDV1=pickle.load(open("../Pickle/cleaved1.p", "rb"))
HDV2=pickle.load(open("../Pickle/cleaved2.p", "rb"))
HDV3=pickle.load(open("../Pickle/cleaved3.p", "rb"))
LIG1=pickle.load(open("../Pickle/ligated1.p", "rb"))
LIG2=pickle.load(open("../Pickle/ligated2.p", "rb"))
LIG3=pickle.load(open("../Pickle/ligated3.p", "rb"))
unHDV1=pickle.load(open("../Pickle/uncleaved1.p", "rb"))
unHDV2=pickle.load(open("../Pickle/uncleaved2.p", "rb"))
unHDV3=pickle.load(open("../Pickle/uncleaved3.p", "rb"))
unLIG1=pickle.load(open("../Pickle/unligated1.p", "rb"))
unLIG2=pickle.load(open("../Pickle/unligated2.p", "rb"))
unLIG3=pickle.load(open("../Pickle/unligated3.p", "rb"))

#%% CALCULATES THE TOTAL READ COUNTS FOR EACH EXPECTED GENOTYPE
HDV1_tot={}
HDV2_tot={}
HDV3_tot={}

for seq in unHDV1:
    HDV1_tot[seq]=HDV1.get(seq,0)+unHDV1[seq]
    HDV2_tot[seq]=HDV2.get(seq,0)+unHDV2[seq]
    HDV3_tot[seq]=HDV3.get(seq,0)+unHDV3[seq]

#%% PLOTS THE DISTRIBUTIONS OF READ COUNTS FOR EACH EXPECTED GENOTYPE (FIG S6)
plt.figure(figsize=[10,6])
plt.subplot(321)
sns.distplot(list(HDV1_tot.values()),color='red',kde=False,bins=100)
plt.title('Replicate 1',weight='bold',fontsize=15)
plt.yticks([0,1000,2000])
plt.ylim(0,2500)
plt.xlim(0,3000)
plt.vlines(np.mean(list(HDV1_tot.values())),linestyle='dashed',ymin=0,ymax=2500)
print(np.mean(list(HDV1_tot.values())))
print(np.median(list(HDV1_tot.values())))
print(stats.mode(list(HDV1_tot.values())))
plt.subplot(323)
sns.distplot(list(HDV2_tot.values()),color='red',kde=False,bins=100)
plt.title('Replicate 2',weight='bold',fontsize=15)
plt.yticks([0,1000,2000])
plt.ylim(0,2500)
plt.xlim(0,3000)
plt.vlines(np.mean(list(HDV2_tot.values())),linestyle='dashed',ymin=0,ymax=2500)
print(np.mean(list(HDV2_tot.values())))
print(np.median(list(HDV2_tot.values())))
print(stats.mode(list(HDV2_tot.values())))
plt.ylabel('genotypes', weight='bold',fontsize=15)
plt.subplot(325)
sns.distplot(list(HDV3_tot.values()),color='red',kde=False,bins=100)
plt.title('Replicate 3',weight='bold',fontsize=15)
plt.yticks([0,1000,2000])
plt.ylim(0,2500)
plt.xlim(0,3000)
plt.vlines(np.mean(list(HDV3_tot.values())),linestyle='dashed',ymin=0,ymax=2500)
print(np.mean(list(HDV3_tot.values())))
print(np.median(list(HDV3_tot.values())))
print(stats.mode(list(HDV3_tot.values())))
plt.xlabel('read counts', weight='bold',fontsize=15)

plt.subplot(322)
sns.distplot(list(unLIG1.values()),color='blue',kde=False,bins=100)
plt.title('Replicate 1',weight='bold',fontsize=15)
plt.yticks([0,2000,4000])
plt.ylim(0,4500)
plt.xlim(0,3000)
plt.vlines(np.mean(list(unLIG1.values())),linestyle='dashed',ymin=0,ymax=4500)
print(np.mean(list(unLIG1.values())))
print(np.median(list(unLIG1.values())))
print(stats.mode(list(unLIG1.values())))
plt.subplot(324)
sns.distplot(list(unLIG2.values()),color='blue',kde=False,bins=100)
plt.title('Replicate 2',weight='bold',fontsize=15)
plt.yticks([0,2000,4000])
plt.ylim(0,4500)
plt.xlim(0,3000)
plt.vlines(np.mean(list(unLIG2.values())),linestyle='dashed',ymin=0,ymax=4500)
print(np.mean(list(unLIG2.values())))
print(np.median(list(unLIG2.values())))
print(stats.mode(list(unLIG2.values())))
plt.subplot(326)
sns.distplot(list(unLIG3.values()),color='blue',kde=False,bins=100)
plt.title('Replicate 3',weight='bold',fontsize=15)
plt.yticks([0,2000,4000])
plt.ylim(0,4500)
plt.xlim(0,3000)
plt.vlines(np.mean(list(unLIG3.values())),linestyle='dashed',ymin=0,ymax=4500)
print(np.mean(list(unLIG3.values())))
print(np.median(list(unLIG3.values())))
print(stats.mode(list(unLIG3.values())))
sns.despine(trim=True)
plt.tight_layout()
plt.xlabel('read counts', weight='bold',fontsize=15)
plt.savefig('../Figures/HDV-LIG14_seq_readcount.png',bbox_inches='tight',dpi=1000)

#%% CALCULATES THE TOTAL READ COUNTS FOR EACH UNEXPECTED GENOTYPE
HDV1_tot={}
HDV2_tot={}
HDV3_tot={}

for seq in HDV['unexpected_uncleaved1']:
    HDV1_tot[seq]=HDV['unexpected_cleaved1'].get(seq,0)+HDV['unexpected_uncleaved1'].get(seq,0)
for seq in HDV['unexpected_uncleaved2']:
    HDV2_tot[seq]=HDV['unexpected_cleaved2'].get(seq,0)+HDV['unexpected_uncleaved2'].get(seq,0)
for seq in HDV['unexpected_uncleaved3']:
    HDV3_tot[seq]=HDV['unexpected_cleaved3'].get(seq,0)+HDV['unexpected_uncleaved3'].get(seq,0)

unLIG1=LIG['unexpected_unligated1']
unLIG2=LIG['unexpected_unligated2']
unLIG3=LIG['unexpected_unligated3']

#%% PLOTS THE DISTRIBUTIONS OF READ COUNTS FOR EACH UNEXPECTED GENOTYPE (FIG S7)
plt.figure(figsize=[10,6])
plt.subplot(321)
sns.distplot(list(HDV1_tot.values()),color='red',kde=False,bins=17)
plt.title('Replicate 1',weight='bold',fontsize=15)
plt.yticks([0,20000,40000])
plt.ylim(0,40000)
plt.xlim(0,60)
plt.vlines(np.mean(list(HDV1_tot.values())),linestyle='dashed',ymin=0,ymax=40000)
plt.subplot(323)
sns.distplot(list(HDV2_tot.values()),color='red',kde=False,bins=15)
plt.title('Replicate 2',weight='bold',fontsize=15)
plt.yticks([0,20000,40000])
plt.ylim(0,40000)
plt.xlim(0,60)
plt.vlines(np.mean(list(HDV2_tot.values())),linestyle='dashed',ymin=0,ymax=40000)
plt.ylabel('unexpected genotypes', weight='bold',fontsize=15)
plt.subplot(325)
sns.distplot(list(HDV3_tot.values()),color='red',kde=False,bins=580)
plt.title('Replicate 3',weight='bold',fontsize=15)
plt.yticks([0,20000,40000])
plt.ylim(0,40000)
plt.xlim(0,60)
plt.vlines(np.mean(list(HDV3_tot.values())),linestyle='dashed',ymin=0,ymax=40000)
plt.xlabel('read counts', weight='bold',fontsize=15)

plt.subplot(322)
sns.distplot(list(unLIG1.values()),color='blue',kde=False,bins=35)
plt.title('Replicate 1',weight='bold',fontsize=15)
plt.yticks([0,50000,100000])
plt.ylim(0,)
plt.xlim(0,60)
plt.vlines(np.mean(list(unLIG1.values())),linestyle='dashed',ymin=0,ymax=100000)
plt.subplot(324)
sns.distplot(list(unLIG2.values()),color='blue',kde=False,bins=35)
plt.title('Replicate 2',weight='bold',fontsize=15)
plt.yticks([0,50000,100000])
plt.ylim(0,)
plt.xlim(0,60)
plt.vlines(np.mean(list(unLIG2.values())),linestyle='dashed',ymin=0,ymax=100000)
plt.subplot(326)
sns.distplot(list(unLIG3.values()),color='blue',kde=False,bins=35)
plt.title('Replicate 3',weight='bold',fontsize=15)
plt.yticks([0,50000,100000])
plt.ylim(0,)
plt.xlim(0,60)
plt.vlines(np.mean(list(unLIG3.values())),linestyle='dashed',ymin=0,ymax=100000)
sns.despine(trim=True)
plt.tight_layout()
plt.xlabel('read counts', weight='bold',fontsize=15)
plt.savefig('../Figures/HDV-LIG14_seq_unexpected_readcount.png',bbox_inches='tight',dpi=1000)