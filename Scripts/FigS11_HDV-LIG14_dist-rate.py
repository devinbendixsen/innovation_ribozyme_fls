#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 11:58:58 2019

@author: devin
"""

import matplotlib.pyplot as plt
import seaborn as sns

#%% CALCULATE THE MUTATIONAL DISTANCE FROM THE STARTING POINT AND THE SUMMIT
LIGsummit='AGGGCGCCTTGCCG'
HDVsummit='CGTCGTGTCCGGAC'
def mutcalc(seq,WT): #determines the number of mutations
    mut=0 #number of mutations from WT
    for i in range(1,len(WT)+1):
        if seq[i-1:i]!=WT[i-1:i]:
            mut+=1
    return mut
LIGstart={'REF':'CGUCGUCCCCGGAC',
'a':'CGUCGUGUCCGGAC',
'b':'CAGCGUGUUCGGCC',
'c':'CGUCGUGUCUGGAC',
'd':'CAUCGUGCCCGGAC',
'e':'CAUCGUCCCCGGAC',
'f':'CGGCGUGUCUGGCC',
'g':'CAUCGUGUCCGGAC',
'h':'CAGCGUCUCUGGCC',
'i':'CAGCGUCUUCGGCC',
'j':'CAUCGUCUUCGGAC',
'k':'CAUCGUGCCUGGAC',
'l':'CAUCGUGUCUGGAC',
'm':'CAUCGUCUCCGGAC',
'n':'CAGCGUGUCUGGCC',
'o':'CGUCGUCCUCGGAC',
'p':'CAUCGUCCUCGGAC'}
HDVstart={
'A':'AGGGCGCCUUGCCG', 
'B':'AGGGCGCCCUGCCG' ,
'C':'AGGGCGCUUUACCG' ,
'D':'AGGGCGCUUCACCG' ,
'E':'AGGGCGCCUCGCCG' ,
'F':'AGGGCGCUCUACCG' ,
'G':'AGGGCGGUUUACCG' ,
'H':'AGGGCGCUUUGCCG' ,
'I':'AAGGCGCUUCACCG' ,
'J':'AGGGCGCCCCGCCG' ,
'K':'AAGGCGCCUCGCCG' ,
'L':'AAGGCGCUCUACCG' ,
'M':'AAGGCGCCCUGCCG' ,
'N':'AAGGCGCUUUACCG' ,
'O':'AAGGCGCCUUGCCG' ,
'P':'AGGGCGCUCUGCCG' ,
'Q':'AAGGCGGCCCGCCG' }

LIGdist={}
for seq in LIGstart:
    LIGdist[seq]=mutcalc(LIGstart[seq],LIGsummit)
HDVdist={}
for seq in HDVstart:
    HDVdist[seq]=mutcalc(HDVstart[seq],HDVsummit)
#%% GROWTH RATES CALCULATED IN FIG S10 AND S16
LIGrate={'REF':0.11,'a':0.06,'b':0.06,'c':0.12,'d':0.07,'e':0.04,'f':0.12,'g':0.03,'h':0.19,'i':0.04,'j':0.05,'k':0.22,'l':0.17,'m':0.03,'n':0.07,'o':0.13,'p':0.10}
HDVrate={'A':0.003, 'B':0.003,'C':0.002 ,'D':0.003 ,'E':0.001 ,'F':0.0 ,'G':0.004 ,'H':0.00 ,'I':0.004 ,'J':0.003 ,'K':0.003 ,'L':0.005 ,'M':0.002 ,'N':0.004 ,'O':0.00 ,'P':0.005,'Q':0.003 }

#%% PLOT THE RELATIONSHIP BETWEEN MUTATIONAL DISTANCE AND GROWTH RATE
plt.figure(figsize=[8,4])
sns.set_palette('Blues_r',8)
plt.subplot(121)

for seq in LIGrate:
    plt.scatter(LIGdist[seq],LIGrate[seq],edgecolor='black',s=50)
sns.despine(trim=True)

plt.yticks([0.00,0.08,0.16,0.24],fontsize=15, weight='bold')
plt.xticks(fontsize=15,weight='bold')
plt.ylabel('growth rate',fontsize=15,weight='bold')
plt.xlabel('mutational distance\nLigase summit',fontsize=15,weight='bold')
plt.ylim(-0.002,)
sns.set_palette('Reds_r',8)
plt.subplot(122)
for seq in HDVrate:
    plt.scatter(HDVdist[seq],HDVrate[seq],edgecolor='black',s=50)
plt.yticks([0.000,0.001,0.002,0.003,0.004,0.005],fontsize=15, weight='bold')
plt.xticks(fontsize=15,weight='bold')
plt.ylabel('growth rate',fontsize=15,weight='bold')
plt.xlabel('mutational distance\nHDV summit',fontsize=15,weight='bold')
plt.ylim(-0.001,0.0051)
sns.despine(trim=True)
plt.tight_layout()

plt.savefig('../Figures/HDV-LIG14_dist-rate.png',bbox_inches='tight',dpi=1000)#,transparent=True)