#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  7 16:10:42 2017

@author: devin
"""
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
sns.set_style("ticks")
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)
sheet='data'
xls=pd.ExcelFile('../DATA/MutDistanceViolinPlots_data.xlsx')
xls.sheet_names
[sheet]
df=xls.parse(sheet)
df.head()
#%%
plt.figure(figsize=(6,4.5))
plt.vlines([1,2,3,4,5,6,7,8,9,10],linestyle='dashed',ymin=-1,ymax=13,zorder=0,linewidth=1.5,color='darkgrey')
ax = sns.violinplot(y="Fitness Requirement", x="Mutational Distance", hue="Initial Function", data=df, palette={'HDV':'r','LIG':'b'}, saturation=0.9,orient='h', split=True, linewidth=1.5, scale="width",inner=None, bw=0.25, cut=0)
plt.xlim(0,)
sns.despine(trim=True)
ax.invert_yaxis()
ax.legend_.remove()
plt.xlabel('mutational distance',weight='bold',fontsize=15)
plt.ylabel('fitness cut-off',weight='bold',fontsize=15)
plt.savefig('../Figures/mutdistance_violinplot.png',bbox_inches='tight',dpi=1000,transparent=True)
