#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 14:27:31 2019

@author: devin
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 20:31:24 2018

@author: devin
"""
import matplotlib.pyplot as plt
import matplotlib as mpl
import pickle
from random import randint
import seaborn as sns
import numpy as np
import scipy
font = {'family' : 'sans-serif','weight' : 'bold','size' : 15}
plt.rc('font', **font)
sns.set_style('ticks')

#%% UPLOADS DATA FROM SIMULATIONS ON THE HDV LANDSCAPE FOLLOWING NEUTRAL EVOLUTION
data_0    = pickle.load( open('../Results/HDV-LIG14_neutral/DATA_sims_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-0.p','rb'))
data_100  = pickle.load( open('../Results/HDV-LIG14_neutral/DATA_sims_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-100.p','rb'))
data_200  = pickle.load( open('../Results/HDV-LIG14_neutral/DATA_sims_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-200.p','rb'))
data_300  = pickle.load( open('../Results/HDV-LIG14_neutral/DATA_sims_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-300.p','rb'))
data_400  = pickle.load( open('../Results/HDV-LIG14_neutral/DATA_sims_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-400.p','rb'))
data_500  = pickle.load( open('../Results/HDV-LIG14_neutral/DATA_sims_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-500.p','rb'))
data_600  = pickle.load( open('../Results/HDV-LIG14_neutral/DATA_sims_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-600.p','rb'))
data_700  = pickle.load( open('../Results/HDV-LIG14_neutral/DATA_sims_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-700.p','rb'))
data_800  = pickle.load( open('../Results/HDV-LIG14_neutral/DATA_sims_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-800.p','rb'))
data_900  = pickle.load( open('../Results/HDV-LIG14_neutral/DATA_sims_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-900.p','rb'))
data_1000 = pickle.load( open('../Results/HDV-LIG14_neutral/DATA_sims_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-1000.p','rb'))

#%% CALCULATES METRICS (FINAL FITNESS AND UNIQUE GENOTYPES EXPLORED)
final_fitness={}
unique_sequences_explored={}
n=1
genotypes=[data_0,data_100,data_200,data_300,data_400,data_500,data_600,data_700,data_800,data_900,data_1000]
for genotype in genotypes:
    final_fitness[n]=genotype['final_fitness']
    unique_sequences_explored[n]=genotype['unique_sequences_explored']
    n+=1

#%% PLOTS THE DISTRIBUTION OF FINAL FITNESS (FIG 5D)
plt.figure(1)
plt.figure(figsize=(6,2))
x=[]
y=[]
replicates=100
sns.set_palette('Reds_r',11)
for genotype in final_fitness:
    for i in range(replicates):
        x.append(genotype)
    z=final_fitness[genotype]
    for num in z:
        y.append(num)
ax = sns.violinplot(x,y,scale='count',bw=0.12,inner=None,order=[1,2,3,4,5,6,7,8,9,10,11],cut=0)
plt.yticks([0.0,0.5,1.0,1.5])
plt.xticks([0,1,2,3,4,5,6,7,8,9,10],['','','','','','','','','','',''])#[0,'',200,'',400,'',600,'',800,'',1000])
plt.xlabel('neutral generations',weight='bold',fontsize=15)
plt.ylabel('final fitness',weight='bold',fontsize=15)
sns.despine(trim=True)
plt.savefig('../Figures/HDV_final_fitness_neutral.png',transparent=True,bbox_inches='tight',dpi=1000)

#%% PLOTS THE DISTRIBUTION OF UNIQUE GENOTYPES EXPLORED (FIG 5D)
plt.figure(2)
plt.figure(figsize=(6,2))
x=[]
y=[]
replicates=100
sns.set_palette('Reds_r',11)
for genotype in unique_sequences_explored:
    for i in range(replicates):
        x.append(genotype)
    z=unique_sequences_explored[genotype]
    for num in z:
        y.append(num)
ax = sns.violinplot(x,y,scale='count',inner=None,bw=0.12)
label=[0,'',200,'',400,'',600,'',800,'',1000]
plt.xticks(list(range(0,11)),label,weight='bold',fontsize=15)
plt.yticks([0,500,1000,1500,2000,2500],weight='bold',fontsize=15)
plt.xlabel('neutral generations',weight='bold',fontsize=15)
plt.ylabel('unique genotypes',weight='bold',fontsize=15)
sns.despine(trim=True, bottom=False)
plt.savefig('../Figures/HDV_unique_explored_neutral.png',transparent=True,bbox_inches='tight',dpi=1000)

#%% UPLOADS POPULATION FITNESS DATA FROM SIMULATIONS ON THE HDV LANDSCAPE FOLLOWING NEUTRAL EVOLUTION
data_0 = pickle.load( open('../Results/HDV-LIG14_neutral/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-0.p','rb'))
data_100 = pickle.load( open('../Results/HDV-LIG14_neutral/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-100.p','rb'))
data_200 = pickle.load( open('../Results/HDV-LIG14_neutral/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-200.p','rb'))
data_300 = pickle.load( open('../Results/HDV-LIG14_neutral/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-300.p','rb'))
data_400 = pickle.load( open('../Results/HDV-LIG14_neutral/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-400.p','rb'))
data_500 = pickle.load( open('../Results/HDV-LIG14_neutral/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-500.p','rb'))
data_600 = pickle.load( open('../Results/HDV-LIG14_neutral/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-600.p','rb'))
data_700 = pickle.load( open('../Results/HDV-LIG14_neutral/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-700.p','rb'))
data_800 = pickle.load( open('../Results/HDV-LIG14_neutral/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-800.p','rb'))
data_900 = pickle.load( open('../Results/HDV-LIG14_neutral/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-900.p','rb'))
data_1000 = pickle.load( open('../Results/HDV-LIG14_neutral/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG_N-1000.p','rb'))

#%% CALCULATE SIMULATION MEANS
replicates=100
genotypes=[data_0,data_100,data_200,data_300,data_400,data_500,data_600,data_700,data_800,data_900,data_1000]
average={}
n=1
for genotype in genotypes:
    average[n]={}
    for i in range(1,1001):
        average[n][i]=[]
    n+=1

n=1
for genotype in genotypes:
    for rep in genotype:
        for i in range(1,1001):
            average[n][i].append(genotype[rep][i-1])
    n+=1

popmean={}
n=1
for genotype in genotypes:
    popmean[n]={}
    for i in range(1,1001):
        popmean[n][i]=[]
    n+=1

n=1
for genotype in genotypes:
    for rep in genotype:
        for i in range(1,1001):
            popmean[n][i]=np.mean(average[n][i])
    n+=1

#%% PLOT MEANS OF SIMULATIONS FROM EACH STARTING POINT (FIG 5B)
order=[1,2,3,4,5,6,7,8,9,10,11]
plt.figure(3)
cmap = mpl.cm.Reds_r
plt.figure(figsize=(6,4))
n=1
for genotype in order:
    plt.plot(popmean[genotype].keys(), popmean[genotype].values(), color=cmap((n)/float(len(popmean))),linewidth=2)
    n+=1
plt.yticks([0,0.5,1.0,1.5],weight='bold',fontsize=15,color='black')
plt.xticks([0,200,400,600,800,1000],weight='bold',fontsize=15,color='black')
sns.despine( right=True)
plt.xlabel('generation',weight='bold', fontsize=15)
plt.ylabel('mean fitness of simulations',weight='bold', fontsize=15)
plt.title('HDV landscape',weight='bold',fontsize=15, color='black')
plt.savefig('../Figures/HDV14_neutral_pop_mean.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% CALCULATES THE ADAPTATION RATE FOLLOWING NEUTRAL EVOLUTION
genotypes=[data_0,data_100,data_200,data_300,data_400,data_500,data_600,data_700,data_800,data_900]
gen_neutral={1:0,2:100,3:200,4:300,5:400,6:500,7:600,8:700,9:800,10:900,11:1000}
initial_rate={}

n=1
for genotype in genotypes:
    initial_rate[n]=[]
    for rep in genotype:
        x=(genotype[rep][gen_neutral[n]+99]-genotype[rep][gen_neutral[n]])/100
        initial_rate[n].append(x)
    n+=1
    
#%% PLOTS THE DISTRIBUTION OF ADAPTATION RATES FOLLOWING NEUTRAL EVOLUTION (FIG 5D)
plt.figure(4)
plt.figure(figsize=(6,2))
x=[]
y=[]
n=1
sns.set_palette('Reds_r',11)
for genotype in genotypes:
    for i in range(replicates):
        x.append(n)
    z=initial_rate[n]
    for num in z:
        y.append(num)
    n+=1
ax = sns.violinplot(x,y,bw=0.1,scale='count',inner=None)
plt.yticks([0.000,0.005,0.010,0.015],weight='bold',fontsize=15)
plt.xlabel('neutral generations',weight='bold', fontsize=15)
plt.ylabel('adaptation rate',weight='bold', fontsize=15)
label=[0,'',200,'',400,'',600,'',800,'',1000]
plt.xticks(list(range(0,11)),label,weight='bold',fontsize=15)
sns.despine(trim=True, bottom=False)
plt.savefig('../Figures/HDV_initial_rate_neutral.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% PLOT ALL THE REPLICATES OF EACH STARTING POINT (FIG S18)
plt.figure(5)
plt.figure(figsize=(7,8))
gen_neutral={1:0,2:100,3:200,4:300,5:400,6:500,7:600,8:700,9:800,10:900,11:1000}
genotypes=[data_0,data_100,data_200,data_300,data_400,data_500,data_600,data_700,data_800,data_900,data_1000]
n=0
for genotype in genotypes:
    n+=1
    cmap=mpl.cm.YlOrRd
    plt.subplot(3,4,n)
    for rep in genotype:
        plt.plot(genotype[rep],color=cmap(randint(1,220)),linewidth=2)
    plt.title(gen_neutral[n],weight='bold',fontsize=12, color='black')
    plt.yticks([0.0,0.5,1.0,1.5],weight='bold',fontsize=12,color='black')
    plt.xticks([0,500,1000],weight='bold',fontsize=12,color='black')
    sns.despine( right=True)
    #plt.axvline(x=200,linestyle='--',color='black')
    if n==9:
        plt.ylabel('population fitness',weight='bold',fontsize=15,color='black')
    if n==9:
        plt.xlabel('generations',weight='bold',fontsize=15,color='black')
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
plt.savefig('../Figures/HDV_neutral_traces.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% PLOTS THE CUBIC LINE REGRESSION OF MEAN SIMULATIONS AND CALCULATES MAXIMUM SLOPE (FIG S20)
start={1:'0',2:'100',3:'200',4:'300',5:'400',6:'500',7:'600',8:'700',9:'800',10:'900',11:'1000'}
sns.set_palette('Reds_r',17)

n=1
plt.figure(6)
plt.figure(figsize=[8,12])
for seq in popmean:
    x=range(0,1000)
    y=list(popmean[seq].values())
    slope_max=0
    yplot=[]
    xplot=[]
    for num in x:
        for num2 in x:
            if num2>num+50:
                slope=((y[num]-y[num2])/(num-num2))
                if slope>=slope_max:
                    slope_max=slope
                    yplot=[y[num2],y[num]]
                    xplot=[num2,num]
    plt.subplot(5,4,seq)
    plt.scatter(x, y,zorder=0,s=10,facecolors='none',edgecolors='indianred')
    cs=scipy.interpolate.CubicSpline(x,y,bc_type='natural')
    plt.plot(x,cs(x),'--',color='black')
    plt.ylim(0,0.4)
    plt.yticks([0,0.1,0.2,0.3,0.4],weight='bold',fontsize=12,color='black')
    plt.xticks([0,500,1000],weight='bold',fontsize=12,color='black')
    plt.title(start[seq],weight='bold',fontsize=12,color='black')
    plt.xlabel('generation',weight='bold', fontsize=12)
    plt.ylabel('fitness',weight='bold', fontsize=12)
    plt.text(50,0.35,'\u03BC={}'.format(round(slope_max,3)),fontsize=10,weight='bold')
    n+=1
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
plt.savefig('../Figures/HDV14_neutral_spline_regression.png',bbox_inches='tight',dpi=1000)