#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 20:31:24 2018

@author: devin
"""
import matplotlib.pyplot as plt
import matplotlib as mpl
import pickle
from random import randint
import seaborn as sns
import numpy as np
import scipy
#%%UPLOAD DATA FROM EVOLUTIONARY SIMULATIONS
a_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTTGCCG.p','rb'))
b_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCCTGCCG.p','rb'))
c_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCTTTACCG.p','rb'))
d_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCTTCACCG.p','rb'))
e_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCTCGCCG.p','rb'))
f_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCTCTACCG.p','rb'))
g_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGGTTTACCG.p','rb'))
h_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCTTTGCCG.p','rb'))
i_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AAGGCGCTTCACCG.p','rb'))
j_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCCCCGCCG.p','rb'))
k_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AAGGCGCCTCGCCG.p','rb'))
l_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AAGGCGCTCTACCG.p','rb'))
m_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AAGGCGCCCTGCCG.p','rb'))
n_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AAGGCGCTTTACCG.p','rb'))
o_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AAGGCGCCTTGCCG.p','rb'))
p_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AGGGCGCTCTGCCG.p','rb'))
q_pop = pickle.load( open('../Results/HDV-LIG14/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-AAGGCGGCCCGCCG.p','rb'))
genotypes=[a_pop,b_pop,c_pop,d_pop,e_pop,f_pop,g_pop,h_pop,i_pop,j_pop,k_pop,l_pop,m_pop,n_pop,o_pop,p_pop,q_pop]

#%% CALCULATE SIMULATION MEANS
average={}
n=1
for genotype in genotypes:
    average[n]={}
    for i in range(1,1001):
        average[n][i]=[]
    n+=1

n=1
for genotype in genotypes:
    for rep in genotype:
        for i in range(1,1001):
            average[n][i].append(genotype[rep][i-1])
    n+=1

popmean={}
n=1
for genotype in genotypes:
    popmean[n]={}
    for i in range(1,1001):
        popmean[n][i]=[]
    n+=1

n=1
for genotype in genotypes:
    for rep in genotype:
        for i in range(1,1001):
            popmean[n][i]=np.mean(average[n][i])
    n+=1

#%% PLOT MEANS OF SIMULATIONS FROM EACH STARTING POINT (FIG 4D)
order=[6,15,8,13,5,3,2,17,1,11,4,10,7,9,14,12,16]
plt.figure(1)
cmap = mpl.cm.Reds
plt.figure(figsize=(4.667,4))
n=1
for genotype in order:
    plt.plot(popmean[genotype].keys(), popmean[genotype].values(), color=cmap((n)/float(len(popmean))),linewidth=4)
    n+=1
plt.yticks([0.0,0.5,1.0,1.5],weight='bold',fontsize=15,color='black')
plt.xticks([0,200,400,600,800,1000],weight='bold',fontsize=15,color='black')
sns.despine( right=True)
plt.axvline(x=200,linestyle='--',color='black')
plt.xlabel('generation',weight='bold',fontsize=15,color='black')
plt.ylabel('mean fitness of simulations',weight='bold',fontsize=15,color='black')
plt.title('HDV landscape',weight='bold',fontsize=15,color='black')
plt.ylim(0.,1.5)
plt.savefig('../Figures/HDV_pop_mean.png',bbox_inches='tight',dpi=1000)

#%% CALCULATE THE INITIAL RATE OF SIMULATIONS
initial_rate={}
n=1
for genotype in genotypes:
    initial_rate[n]=[]
    for rep in genotype:
        x=(genotype[rep][199]-genotype[rep][0])/200
        initial_rate[n].append(x)
    n+=1

rate_mean={}
for genotype in initial_rate:
    rate_mean[genotype] = np.mean(initial_rate[genotype])

#%% PLOT THE DISTRIBUTIONS OF INITIAL RATES OF SIMULATIONS FROM EACH STARTING POINT (FIG 4G)
start={1:'REF',2:'a',3:'b',4:'c',5:'d',6:'e',7:'f',8:'g',9:'h',10:'i',11:'j',12:'k',13:'l',14:'m',15:'n',16:'o',17:'p'}
replicates=100
plt.figure(2)
plt.figure(figsize=(12,2))
x=[]
y=[]
replicates=100
n=1
sns.set_palette('Reds_r',17)
for genotype in genotypes:
    for i in range(replicates):
        x.append(n)
    z=initial_rate[n]
    for num in z:
        y.append(num)
    n+=1
ax = sns.violinplot(x,y,bw=0.15,scale='count',inner='stick',order=[16,12,14,9,7,10,4,11,1,17,2,3,5,13,8,15,6])

label=[]
for seq in order:
    label.append(start[seq])
    
plt.xlabel('starting genotypes',weight='bold', fontsize=15)
plt.ylabel('initial rate',weight='bold', fontsize=15)
plt.xticks(list(range(0,17)),['P','L','N','I','G','J','D','K','A','Q','B','C','E','M','H','O','F'],weight='bold',fontsize=15)
plt.yticks([0.0000,0.0005,0.0010,0.0015],weight='bold',fontsize=15)
sns.despine(trim=True, bottom=False)
plt.savefig('../Figures/HDV_initial_rate.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% PLOT ALL THE REPLICATES OF A GIVEN STARTING POINT (FIG 4C, FIG S15)
plt.figure(3)
genotypes=[a_pop,b_pop,c_pop,d_pop,e_pop,f_pop,g_pop,h_pop,i_pop,j_pop,k_pop,l_pop,m_pop,n_pop,o_pop,p_pop,q_pop]
plt.figure(figsize=(3,4))
cmap=mpl.cm.YlOrRd
start=a_pop
for rep in start:
    plt.plot(start[rep],color=cmap(randint(1,220)),linewidth=4)
plt.xticks([0,250,500,750,1000],weight='bold',fontsize=15,color='black')
plt.yticks([0,0.5,1.0,1.5],weight='bold',fontsize=15,color='black')
sns.despine( right=True)
plt.savefig('../Figures/a_pop.png',bbox_inches='tight',dpi=1000)

#%% PLOT THE REGRESSION ANALYSIS AND DETERMINE THE MAXIMUM SLOPE (FIG S16)
start={1:'A',2:'B',3:'C',4:'D',5:'E',6:'F',7:'G',8:'H',9:'I',10:'J',11:'K',12:'L',13:'M',14:'N',15:'O',16:'P',17:'Q'}
sns.set_palette('Reds_r',17)

n=1
plt.figure(4)
plt.figure(figsize=(8,12))
for seq in popmean:
    x=range(0,1000)
    y=list(popmean[seq].values())
    slope_max=0
    yplot=[]
    xplot=[]
    for num in x:
        for num2 in x:
            if num2>num+50:
                slope=((y[num]-y[num2])/(num-num2))
                if slope>=slope_max:
                    slope_max=slope
                    yplot=[y[num2],y[num]]
                    xplot=[num2,num]
    plt.subplot(5,4,seq)
    plt.scatter(x, y,zorder=0,s=10,facecolors='none',edgecolors='indianred')
    cs=scipy.interpolate.CubicSpline(x,y,bc_type='natural')
    plt.plot(x,cs(x),'--',color='black')
    plt.ylim(0,0.4)
    plt.yticks([0,0.1,0.2,0.3,0.4],weight='bold',fontsize=12,color='black')
    plt.xticks([0,500,1000],weight='bold',fontsize=12,color='black')
    plt.title(start[seq],weight='bold',fontsize=12,color='black')
    plt.xlabel('generation',weight='bold', fontsize=12)
    plt.ylabel('fitness',weight='bold', fontsize=12)
    plt.text(400,0.05,'\u03BC={}'.format(round(slope_max,3)),fontsize=10,weight='bold')
    n+=1
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
plt.savefig('../Figures/HDV14_spline_regression.png',bbox_inches='tight',dpi=1000)