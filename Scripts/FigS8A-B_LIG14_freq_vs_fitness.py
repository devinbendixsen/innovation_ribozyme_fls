#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 16:14:39 2018

@author: devin
"""


# =============================================================================
# code to generate figure depicting ligase pre-selection frequency and fitness
# =============================================================================

import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import matplotlib as mpl
import pandas as pd
from scipy import stats
mpl.rcParams['agg.path.chunksize'] = 10000
font = {'family' : 'sans-serif','weight' : 'bold','size'   : 13}
plt.rc('font', **font)

#%% LOAD DATA
ligase=pickle.load(open("../Pickle/normLIGunLIG.p","rb"))
ligated1=pickle.load(open("../Pickle/ligated1.p","rb"))
ligated2=pickle.load(open("../Pickle/ligated2.p","rb"))
ligated3=pickle.load(open("../Pickle/ligated3.p","rb"))
unligated1=pickle.load(open("../Pickle/unligated1.p","rb"))
unligated2=pickle.load(open("../Pickle/unligated2.p","rb"))
unligated3=pickle.load(open("../Pickle/unligated3.p","rb"))
ligase=pickle.load(open("../Pickle/normLIGunLIG.p","rb"))

#%% PLOT CORRELATION BETWEEN LIGASE FITNESS AND FREQUENCY PRE-SELECTION
def metric_correlate(lib1,lib2):
    plt.figure(figsize=[3,3])
    total=sum(list(lib2.values()))
    libprop={}
    for seq in lib2:
        libprop[seq]=lib2[seq]/total
    lib2=libprop
    x=[]
    y=[]
    for genotype in lib1:
        x.append(lib1[genotype])
        y.append(lib2.get(genotype,0))
    datas=pd.DataFrame({'lib1':x,'lib2':y})
    g=sns.JointGrid(x='lib1',y='lib2',data=datas)
    g = g.plot_joint(plt.scatter, color='blue',edgecolor="white")
    g = g.plot_marginals(sns.distplot, color="blue")
    g = g.plot_joint(sns.regplot, color='blue')
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    plt.yticks(weight='bold',fontsize=13,color='black')
    plt.xticks(weight='bold',fontsize=13,color='black')
    plt.ylabel('frequency pre-selection',weight='bold',fontsize=13,color='black')
    plt.xlabel('ligase fitness',weight='bold',fontsize=13,color='black')
    plt.ylim(-0.00001,0.0014)
    print ('R^2 : ', r_value**2)
    print('p-value :',p_value)
    plt.savefig('../Figures/frequency_rep1.png',bbox_inches='tight',dpi=1000,transparent=True) #saves figure
metric_correlate(ligase,unligated1)
metric_correlate(ligase,unligated2)
metric_correlate(ligase,unligated3)
#%% DETERMINES PRE-SELECTION RANK (1-16,384)
rank={}
import csv
reader = csv.reader(open('../DATA/Ligase_data.csv'))

for row in reader:
    x=int(row[1])
    rank[row[0]]=x
#%% PLOTS CORRELATION BETWEEN PRE-SELECTION RANK AND LIGASE FITNESS
plt.figure(figsize=[3,3])
x=[]
y=[]
for genotype in ligase:
    x.append(ligase[genotype])
    y.append(rank[genotype])
datas=pd.DataFrame({'lib1':x,'lib2':y})
g=sns.JointGrid(x='lib1',y='lib2',data=datas)
g = g.plot_joint(plt.scatter, color='blue',edgecolor="white")
g = g.plot_marginals(sns.distplot, color="blue")
g = g.plot_joint(sns.regplot, color='blue')
slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
plt.yticks(weight='bold',fontsize=13,color='black')
plt.xticks(weight='bold',fontsize=13,color='black')
plt.ylabel('rank pre-selection',weight='bold',fontsize=13,color='black')
plt.xlabel('ligase fitness',weight='bold',fontsize=13,color='black')
plt.ylim(0,16384)
print ('R^2 : ', r_value**2)
print('p-value :',p_value)
plt.savefig('../Figures/rank_ligase.png',bbox_inches='tight',dpi=1000,transparent=True) #saves figure
