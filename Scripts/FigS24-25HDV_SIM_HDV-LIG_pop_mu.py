#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 12:23:41 2018

@author: devin
"""

import matplotlib.pyplot as plt
import pickle
import seaborn as sns
import numpy as np
size=[25,50,125,250,500,1000]
rate=[0.0001,0.01,0.1,1.0]
#%% LOADS SIMULATION METRIC DATA
DATA={}
for pop in size:
    for mu in rate:
        x='../Results/HDV-pop-mu/DATA_sims_L-HDV14_P-{}_M-{}_G-1000_S-AGGGCGCCTTGCCG.p'.format(pop,mu)
        y=pickle.load( open(x,'rb'))
        z='HDV'+'_'+str(pop)+'_'+str(mu)
        DATA[z]=y

final_fitness={}
unique_sequences_explored={}

for genotype in DATA:
    final_fitness[genotype]=DATA[genotype]['final_fitness']
    unique_sequences_explored[genotype]=DATA[genotype]['unique_sequences_explored']

#%% DETERMINES PLOTTING COLORS AND PLOTTING ORDER
colorlib={'HDV_1000_0.0001':plt.cm.YlOrRd(250),'HDV_1000_0.01':plt.cm.YlOrRd(250),'HDV_1000_0.1':plt.cm.YlOrRd(250),'HDV_1000_1.0':plt.cm.YlOrRd(250),
       'HDV_500_0.0001':plt.cm.YlOrRd(200),'HDV_500_0.01':plt.cm.YlOrRd(200),'HDV_500_0.1':plt.cm.YlOrRd(200),'HDV_500_1.0':plt.cm.YlOrRd(200),
       'HDV_250_0.0001':plt.cm.YlOrRd(150),'HDV_250_0.01':plt.cm.YlOrRd(150),'HDV_250_0.1':plt.cm.YlOrRd(150),'HDV_250_1.0':plt.cm.YlOrRd(150),
       'HDV_125_0.0001':plt.cm.YlOrRd(100),'HDV_125_0.01':plt.cm.YlOrRd(100),'HDV_125_0.1':plt.cm.YlOrRd(100),'HDV_125_1.0':plt.cm.YlOrRd(100),
       'HDV_50_0.0001':plt.cm.YlOrRd(50),'HDV_50_0.01':plt.cm.YlOrRd(50),'HDV_50_0.1':plt.cm.YlOrRd(50),'HDV_50_1.0':plt.cm.YlOrRd(50),
       'HDV_25_0.0001':plt.cm.YlOrRd(0),'HDV_25_0.01':plt.cm.YlOrRd(0),'HDV_25_0.1':plt.cm.YlOrRd(0),'HDV_25_1.0':plt.cm.YlOrRd(0)}
plotorder=['HDV_25_0.0001', 'HDV_50_0.0001','HDV_125_0.0001','HDV_250_0.0001','HDV_500_0.0001','HDV_1000_0.0001', 'HDV_25_0.01','HDV_50_0.01', 'HDV_125_0.01','HDV_250_0.01','HDV_500_0.01','HDV_1000_0.01','HDV_25_0.1', 'HDV_50_0.1','HDV_125_0.1','HDV_250_0.1','HDV_500_0.1','HDV_1000_0.1', 'HDV_25_1.0', 'HDV_50_1.0', 'HDV_125_1.0', 'HDV_250_1.0', 'HDV_500_1.0', 'HDV_1000_1.0']

#%% PLOT DISTRIBUTION OF FINAL FITNESS
plt.figure(1,figsize=(8,6))
x=[]
y=[]
replicates=100
for genotype in final_fitness:
    for i in range(replicates):
        x.append(genotype)
    z=final_fitness[genotype]
    for num in z:
        y.append(num)
ax=sns.violinplot(x,y,scale='width',inner=None, palette=colorlib,order=plotorder,bw=0.25,linewidth=1)
plt.xticks([],[],weight='bold',fontsize=15,color='black')
plt.yticks(weight='bold',fontsize=15,color='black')
plt.savefig('../Figures/HDV_fitness.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% PLOTS DISTRIBUTION OF UNIQUE SEQUENCES EXPLORED
plt.figure(2,figsize=(8,6))
x=[]
y=[]
replicates=100
for genotype in unique_sequences_explored:
    for i in range(replicates):
        x.append(genotype)
    z=unique_sequences_explored[genotype]
    for num in z:
        y.append(num)
ax=sns.violinplot(x,y,scale='width',inner=None, palette=colorlib,order=plotorder,bw=0.25,linewidth=1)
plt.xticks([],[],weight='bold',fontsize=15,color='black')
plt.yticks(weight='bold',fontsize=15,color='black')
plt.savefig('../Figures/HDV_genotypes_explored.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% LOADS POPULATION FITNESS FROM SIMULATION DATA
DATA_pop={}
for pop in size:
    for mu in rate:
        x='../Results/HDV-pop-mu/gen_fit_plot_L-HDV14_P-{}_M-{}_G-1000_S-AGGGCGCCTTGCCG.p'.format(pop,mu)
        y=pickle.load( open(x,'rb'))
        z='HDV'+'_'+str(pop)+'_'+str(mu)
        DATA_pop[z]=y

gen=1000
replicates=100

#%% CALCULATES THE MEAN OF SIMULATIONS
average={}
for genotype in DATA_pop:
    average[genotype]={}
    for i in range(1,1001):
        average[genotype][i]=[]

for genotype in DATA_pop:
    for rep in DATA_pop[genotype]:
        for i in range(1,1001):
            average[genotype][i].append(DATA_pop[genotype][rep][i-1])

popmean={}
for genotype in DATA_pop:
    popmean[genotype]={}
    for i in range(1,1001):
        popmean[genotype][i]=0

for genotype in average:
    for rep in average[genotype]:
        for i in range(1,1001):
            popmean[genotype][i]=(np.mean(average[genotype][i]))

#%% PLOTS THE MEAN POPULATION FITNESS FOR EACH POPULATION SIZE AND MUTATION RATE
mu1=['HDV_1000_1.0','HDV_500_1.0','HDV_250_1.0','HDV_125_1.0','HDV_50_1.0','HDV_25_1.0']
mu2=['HDV_1000_0.1','HDV_500_0.1','HDV_250_0.1','HDV_125_0.1','HDV_50_0.1','HDV_25_0.1']
mu3=['HDV_1000_0.01','HDV_500_0.01','HDV_250_0.01','HDV_125_0.01','HDV_50_0.01','HDV_25_0.01']
mu4=['HDV_1000_0.0001','HDV_500_0.0001','HDV_250_0.0001','HDV_125_0.0001','HDV_50_0.0001','HDV_25_0.0001']

plt.figure(3)
plt.figure(figsize=(8,3))
for genotype in mu1:
    plt.plot(popmean[genotype].keys(), popmean[genotype].values(),color=colorlib[genotype] ,linewidth=3)
plt.xticks([0,200,400,600,800,1000],weight='bold',fontsize=15,color='black')
sns.despine( right=True)
plt.axvline(x=200,linestyle='--',color='black')
plt.savefig('../Figures/HDV_pop-mu_mean_1.0.png',bbox_inches='tight',dpi=1000,transparent=True)

plt.figure(4)
plt.figure(figsize=(8,3))
for genotype in mu2:
    plt.plot(popmean[genotype].keys(), popmean[genotype].values(),color=colorlib[genotype] ,linewidth=3)
plt.xticks([0,200,400,600,800,1000],weight='bold',fontsize=15,color='black')
sns.despine( right=True)
plt.axvline(x=200,linestyle='--',color='black')
plt.savefig('../Figures/HDV_pop-mu_mean_0.1.png',bbox_inches='tight',dpi=1000,transparent=True)

plt.figure(5)
plt.figure(figsize=(8,3))
for genotype in mu3:
    plt.plot(popmean[genotype].keys(), popmean[genotype].values(),color=colorlib[genotype] ,linewidth=3)
plt.xticks([0,200,400,600,800,1000],weight='bold',fontsize=15,color='black')
sns.despine( right=True)
plt.axvline(x=200,linestyle='--',color='black')
plt.savefig('../Figures/HDV_pop-mu_mean_0.01.png',bbox_inches='tight',dpi=1000,transparent=True)

plt.figure(6)
plt.figure(figsize=(8,3))
for genotype in mu4:
    plt.plot(popmean[genotype].keys(), popmean[genotype].values(),color=colorlib[genotype] ,linewidth=3)
plt.xticks([0,200,400,600,800,1000],weight='bold',fontsize=15,color='black')
sns.despine( right=True)
plt.axvline(x=200,linestyle='--',color='black')
plt.savefig('../Figures/HDV_pop-mu_mean_0.0001.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% CALCULATES THE INITIAL RATE
initial_rate={}
for genotype in DATA_pop:
    initial_rate[genotype]=[]
    for rep in DATA_pop[genotype]:
        x=(DATA_pop[genotype][rep][199]-DATA_pop[genotype][rep][0])/200
        initial_rate[genotype].append(x)

rate_mean={}
for genotype in initial_rate:
    rate_mean[genotype] = np.mean(initial_rate[genotype])
#%%
plt.figure(7,figsize=(8,6))
x=[]
y=[]
replicates=100
for genotype in initial_rate:
    for i in range(replicates):
        x.append(genotype)
    z=initial_rate[genotype]
    for num in z:
        y.append(num)
ax=sns.violinplot(x,y,scale='width',inner=None, palette=colorlib,order=plotorder,bw=0.1,linewidth=1)

plt.xticks([],[],weight='bold',fontsize=15,color='black')
plt.yticks(weight='bold',fontsize=15,color='black')
plt.savefig('../Figures/HDV_pop-mu_initial_rate.png',bbox_inches='tight',dpi=1000,transparent=True)