#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 09:48:59 2018

@author: devin
"""
import matplotlib.pyplot as plt
import matplotlib as mpl
import pickle
import seaborn as sns
import numpy as np

#%% LOADS TOTAL FITNESS SIMULATION DATA
HDV=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
LIG=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]

DATA_pop={}
for num in HDV:
    for num2 in LIG:
        if num+num2==1:
            x='../Results/HDV-LIG_co-select/gen_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-mut7_startpop-HDV-{}-LIG-{}.p'.format(num,num2)
            y=pickle.load( open(x,'rb'))
            z=str(num)+'_'+str(num2)
            DATA_pop[z]=y

average={}
for genotype in DATA_pop:
    average[genotype]={}
    for i in range(1,1001):
        average[genotype][i]=[]

for genotype in DATA_pop:
    for rep in DATA_pop[genotype]:
        for i in range(1,1001):
            average[genotype][i].append(DATA_pop[genotype][rep][i-1])

popmean={}
for genotype in DATA_pop:
    popmean[genotype]={}
    for i in range(1,1001):
        popmean[genotype][i]=0

for genotype in average:
    for rep in average[genotype]:
        for i in range(1,1001):
            popmean[genotype][i]=(np.mean(average[genotype][i]))

#PLOT MEAN TOTAL POPULATION FITNESS (FIG S21)
cmap=mpl.cm.RdBu_r
plotcolor={'0.0_1.0':cmap(0),'0.1_0.9':cmap(25),'0.2_0.8':cmap(50),'0.3_0.7':cmap(75),'0.4_0.6':cmap(100),'0.5_0.5':'grey','0.6_0.4':cmap(156),'0.7_0.3':cmap(181),'0.8_0.2':cmap(206),'0.9_0.1':cmap(231),'1.0_0.0':cmap(256)}
plt.figure(1)
plt.figure(figsize=(12,12))
plt.subplot(311)
for genotype in popmean:
    plt.plot(popmean[genotype].keys(), popmean[genotype].values(),color=plotcolor[genotype] ,linewidth=3)
    plt.yticks(weight='bold',fontsize=15,color='black')
    plt.xticks([0,200,400,600,800,1000],weight='bold',fontsize=15,color='black')
    plt.xlim(0,1000)
    plt.ylim(0,1)
    sns.despine( right=True)
    plt.ylabel('total population fitness',weight='bold',fontsize=15,color='black')

#LOADS HDV FITNESS SIMULATION DATA
HDV=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
LIG=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]

HDV_pop={}
for num in HDV:
    for num2 in LIG:
        if num+num2==1:
            x='../Results/HDV-LIG_co-select/HDV_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-mut7_startpop-HDV-{}-LIG-{}.p'.format(num,num2)
            y=pickle.load( open(x,'rb'))
            z=str(num)+'_'+str(num2)
            HDV_pop[z]=y

average={}
for genotype in HDV_pop:
    average[genotype]={}
    for i in range(1,1001):
        average[genotype][i]=[]

for genotype in HDV_pop:
    for rep in HDV_pop[genotype]:
        for i in range(1,1001):
            average[genotype][i].append(HDV_pop[genotype][rep][i-1])

popmean={}
for genotype in HDV_pop:
    popmean[genotype]={}
    for i in range(1,1001):
        popmean[genotype][i]=0

for genotype in average:
    for rep in average[genotype]:
        for i in range(1,1001):
            popmean[genotype][i]=(np.mean(average[genotype][i]))

#PLOTS MEAN HDV FITNESS (FIG S21)
plt.subplot(312)
for genotype in popmean:
    plt.plot(popmean[genotype].keys(), popmean[genotype].values(),color=plotcolor[genotype] ,linewidth=3)
    plt.yticks(weight='bold',fontsize=15,color='black')
    plt.xticks([0,200,400,600,800,1000],weight='bold',fontsize=15,color='black')
    plt.xlim(0,1000)
    plt.ylim(0,1)
    sns.despine( right=True)
    plt.ylabel('HDV population fitness',weight='bold',fontsize=15,color='black')
    
# LOADS LIGASE FITNESS SIMULATION DATA
HDV=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
LIG=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]

LIG_pop={}
for num in HDV:
    for num2 in LIG:
        if num+num2==1:
            x='../Results/HDV-LIG_co-select/LIG_fit_plot_L-HDV14_P-1000_M-0.01_G-1000_S-mut7_startpop-HDV-{}-LIG-{}.p'.format(num,num2)
            y=pickle.load( open(x,'rb'))
            z=str(num)+'_'+str(num2)
            LIG_pop[z]=y

average={}
for genotype in LIG_pop:
    average[genotype]={}
    for i in range(1,1001):
        average[genotype][i]=[]

for genotype in LIG_pop:
    for rep in LIG_pop[genotype]:
        for i in range(1,1001):
            average[genotype][i].append(LIG_pop[genotype][rep][i-1])

popmean={}
for genotype in LIG_pop:
    popmean[genotype]={}
    for i in range(1,1001):
        popmean[genotype][i]=0

for genotype in average:
    for rep in average[genotype]:
        for i in range(1,1001):
            popmean[genotype][i]=(np.mean(average[genotype][i]))

# PLOTS MEAN LIGASE FITNESS (FIG S21)
plt.subplot(313)
for genotype in popmean:
    plt.plot(popmean[genotype].keys(), popmean[genotype].values(),color=plotcolor[genotype] ,linewidth=3)
    plt.yticks(weight='bold',fontsize=15,color='black')
    plt.xticks([0,200,400,600,800,1000],weight='bold',fontsize=15,color='black')
    plt.xlim(0,1000)
    plt.ylim(0,1)
    sns.despine( right=True)
    plt.ylabel('LIG population fitness',weight='bold',fontsize=15,color='black')
    plt.xlabel('generations',weight='bold',fontsize=15,color='black')
plt.savefig('../Figures/HDV-LIG_co-select_meantraces.png',bbox_inches='tight',dpi=1000,transparent=True)

#%% PLOTS INDIVIDUAL TRACES FOR EACH HDV-LIG LANDSCAPE (FIG S22)
# In order to change the HDV-LIG landscape adjust the weights
# landsape variable is in the form of HDVweight_LIGweight
HDVweight=0.0
LIGweight=1.0
landscape='{}_{}'.format(HDVweight,LIGweight)
cmap=mpl.cm.RdBu_r
plt.figure(2,figsize=(12,4))

plt.subplot(131)
start=DATA_pop[landscape]
color=plotcolor[landscape]
for rep in start:
    plt.plot(start[rep],color=color,linewidth=2)
plt.yticks(weight='bold',fontsize=15,color='black')
plt.xticks([0,500,1000],weight='bold',fontsize=15,color='black')
plt.ylim(0,1)
plt.ylabel('population fitness',weight='bold',fontsize=15,color='black')
plt.xlabel('generations',weight='bold',fontsize=15,color='black')
plt.title('total fitness',weight='bold',fontsize=15,color='black')
sns.despine( right=True)

plt.subplot(132)
start=HDV_pop[landscape]
for rep in start:
    plt.plot(start[rep],color=color,linewidth=2)
plt.yticks(weight='bold',fontsize=15,color='black')
plt.xticks([0,500,1000],weight='bold',fontsize=15,color='black')
plt.xlabel('generations',weight='bold',fontsize=15,color='black')
sns.despine( right=True)
plt.ylim(0,1)
plt.title('HDV fitness',weight='bold',fontsize=15,color='black')

plt.subplot(133)
start=LIG_pop[landscape]
for rep in start:
    plt.plot(start[rep],color=color,linewidth=2)
plt.yticks(weight='bold',fontsize=15,color='black')
plt.xticks([0,500,1000],weight='bold',fontsize=15,color='black')
plt.xlabel('generations',weight='bold',fontsize=15,color='black')
sns.despine( right=True)
plt.ylim(0,1)
plt.title('LIG fitness',weight='bold',fontsize=15,color='black')
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
plt.savefig('../Figures/HDV-LIG_co-select_{}.png'.format(landscape),bbox_inches='tight',dpi=1000,transparent=True)
