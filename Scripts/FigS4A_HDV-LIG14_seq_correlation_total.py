#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 14:20:59 2019

@author: devin
"""

import pickle
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
import numpy
font = {'family' : 'sans-serif','weight' : 'bold','size'   : 15}
plt.rc('font', **font)
#%% IMPORTS DATA DERIVED FROM SEQUENCING DATA

mutnum=pickle.load(open("../Pickle/mutnumHDVLIGLib14.p", "rb"))
HDVtotal=pickle.load(open("../Pickle/normHDVtotal.p","rb"))
HDV1=pickle.load(open("../Pickle/cleaved1.p", "rb"))
HDV2=pickle.load(open("../Pickle/cleaved2.p", "rb"))
HDV3=pickle.load(open("../Pickle/cleaved3.p", "rb"))
LIG1=pickle.load(open("../Pickle/ligated1.p", "rb"))
LIG2=pickle.load(open("../Pickle/ligated2.p", "rb"))
LIG3=pickle.load(open("../Pickle/ligated3.p", "rb"))
unHDV1=pickle.load(open("../Pickle/uncleaved1.p", "rb"))
unHDV2=pickle.load(open("../Pickle/uncleaved2.p", "rb"))
unHDV3=pickle.load(open("../Pickle/uncleaved3.p", "rb"))
unLIG1=pickle.load(open("../Pickle/unligated1.p", "rb"))
unLIG2=pickle.load(open("../Pickle/unligated2.p", "rb"))
unLIG3=pickle.load(open("../Pickle/unligated3.p", "rb"))


#%%
HDV1_tot={}
HDV2_tot={}
HDV3_tot={}
LIG1_tot={}
LIG2_tot={}
LIG3_tot={}
for seq in unHDV1:
    HDV1_tot[seq]=HDV1.get(seq,0)+unHDV1[seq]
    HDV2_tot[seq]=HDV2.get(seq,0)+unHDV2[seq]
    HDV3_tot[seq]=HDV3.get(seq,0)+unHDV3[seq]
    LIG1_tot[seq]=LIG1.get(seq,0)+unLIG1.get(seq,0)
    LIG2_tot[seq]=LIG2.get(seq,0)+unLIG2.get(seq,0)
    LIG3_tot[seq]=LIG3.get(seq,0)+unLIG3.get(seq,0)
#%%
def seq_correlate(seq1,seq2,xlabel,ylabel,color):
    plt.figure(figsize=[2,2])
    xlist=[]
    ylist=[]
    xnum=[]
    ynum=[]
    for seq in seq1:
        xnum.append(seq1[seq])
        ynum.append(seq2[seq])
        xlist.append(numpy.log10(seq1[seq])+1)
        ylist.append(numpy.log10(seq2[seq])+1)
    datas=pd.DataFrame({xlabel:xlist,ylabel:ylist})
    g=sns.JointGrid(x=xlabel,y=ylabel,data=datas)
    g = g.plot_joint(plt.scatter, color=color,edgecolor="white")
    g = g.plot_marginals(sns.distplot, color=color)
    g = g.plot_joint(sns.regplot, color=color)
    plt.xlim(0,)
    plt.ylim(0,)
    plt.xlabel(xlabel,weight='bold',fontsize=25)
    plt.ylabel(ylabel,weight='bold',fontsize=25)
    slope, intercept, r_value, p_value, std_err = stats.linregress(xnum, ynum)
    rho, pval = stats.spearmanr(xnum,ynum)
    print(xlabel,ylabel)
    print('Spear :', rho, pval)
    print ('R^2 : ', r_value**2,p_value)
    plt.savefig('../Figures/HDV-LIG14_corr_{}_{}_{}.png'.format(color,xlabel,ylabel),bbox_inches='tight',dpi=1000)
#%%
seq_correlate(HDV1_tot,HDV2_tot,'Replicate 1','Replicate 2','red')
seq_correlate(HDV2_tot,HDV3_tot,'Replicate 2','Replicate 3','red')
seq_correlate(HDV1_tot,HDV3_tot,'Replicate 1','Replicate 3','red')
seq_correlate(LIG1_tot,LIG2_tot,'Replicate 1','Replicate 2','blue')
seq_correlate(LIG2_tot,LIG3_tot,'Replicate 2','Replicate 3','blue')
seq_correlate(LIG1_tot,LIG3_tot,'Replicate 1','Replicate 3','blue')
