#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  7 22:03:38 2018

@author: devin
"""
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)
HDV_epi=pickle.load(open("../Pickle/HDV14_epistasis.p", "rb"))
LIG_epi=pickle.load(open("../Pickle/LIG14_epistasis.p", "rb"))

#%%
plt.figure(figsize=[1,3])
x=[]
y=[]
for seq in HDV_epi:
    if seq>=0:
       x.append('HDV')
       y.append(seq)
sns.violinplot(x,y,color='red',linewidth=2,cut=0,bw=0.25)
plt.ylim(0.1,5)
plt.yticks([0,1,2,3,4,5])
plt.hlines(max(HDV_epi),-.4,.4,linestyles='dashed',linewidth=2)
sns.despine(offset=10, trim=True)
plt.yticks(color='black')
plt.xticks([])
plt.ylabel('epistasis',weight='bold',fontsize=15,color='black')
plt.savefig('../Figures/HDV14_epistasis_analysis_violin.png',bbox_inches='tight',dpi=1000,transparent=True)

plt.figure(figsize=[1,3])
x=[]
y=[]
for seq in LIG_epi:
    if seq>=0:
        x.append('LIG')
        y.append(seq)
sns.violinplot(x,y,color='blue',linewidth=2,cut=0,bw=0.25)
plt.ylim(0.1,5)
plt.yticks([0,1,2,3,4,5])
plt.hlines(max(HDV_epi),-.4,.4,linestyles='dashed',linewidth=2)
sns.despine(offset=10, trim=True)
plt.yticks(color='black')
plt.xticks([])
plt.ylabel('epistasis',weight='bold',fontsize=15,color='black')
plt.savefig('../Figures/LIG14_epistasis_analysis_violin.png',bbox_inches='tight',dpi=1000,transparent=True)