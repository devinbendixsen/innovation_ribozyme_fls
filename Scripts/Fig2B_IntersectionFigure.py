#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 16 13:40:14 2017

@author: devin
"""
import pickle
import numpy
import matplotlib.pyplot as plt
mutnum=pickle.load(open("../Pickle/mutnumHDVLIGLib14.p", "rb"))
HDV=pickle.load(open("../Pickle/normHDVtotal.p", "rb"))
LIG=pickle.load(open("../Pickle/normLIGunLIG.p", "rb"))
connections=pickle.load(open("../Pickle/2wayconnections.p", "rb"))
font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 15}
plt.rc('font', **font)
#%%
plt.figure(figsize=(4,4))
intersectionseq=[]
HDVlist=[]
LIGlist=[]
intersection={}
for key in mutnum:
    if HDV[key]>0 and LIG[key]>0:
        intersection[(HDV[key])]=(LIG[key])
        intersectionseq.append(key)

numred=0
numblue=0
for key in intersection:
    red=(key*10)
    if red>1:
        red=1
    blue=(intersection[key]*20)
    if blue>1:
        blue=1
    size=(red/5.5)+(blue/8)
    color=(red,0,blue)
    plt.scatter(numpy.log10(key),numpy.log10(intersection[key]),c=color,s=size*150,edgecolor='none',marker='.')
plt.xlabel('log10 (HDV fitness)',fontsize=15, fontweight='bold')
plt.ylabel('log10 (Ligase fitness)',fontsize=15, fontweight='bold')

plt.xticks([-3,-2,-1,0,1])
plt.yticks([-3,-2,-1,0,1])
x=numpy.log10(HDV['CAGCGTCCTTGGCC'])
y=numpy.log10(LIG['CAGCGTCCTTGGCC'])
plt.scatter(x,y,c='mistyrose',s=35, marker='D',edgecolor="black")
plt.xlim(-2.25,0.2)
#plt.xlim(-3.75,1.4)
plt.ylim(-3.75,1.4)
plt.axvline(0, linestyle='dashed', linewidth=2, color='black', alpha=1)
plt.axhline(0, linestyle='dashed', linewidth=2, color='black', alpha=1)
plt.savefig('../Figures/intersection_figure.png',bbox_inches='tight',dpi=1000,transparent=True)