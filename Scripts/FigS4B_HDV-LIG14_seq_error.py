#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 13 14:32:51 2018

@author: devin
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

#%% UPLOADS SEQUENCING STATISTICS FILE
excel_file = '../DATA/seq_stat.xlsx'
df = pd.read_excel(excel_file)
x=df[df['lane']==1]

seq_error={}
for i in range(1,170):
    seq_error[i]=[]

for index, row in x.iterrows():
    seq_error[row["cycle"]].append(row["error_rate"])

error_mean={}
error_std={}

for num in seq_error:
    error_mean[num]=np.mean(seq_error[num])
    error_std[num]=np.std(seq_error[num])

phase=[10,13,16,19]
mutnum=[13,23,28,38,52,55,65,68,72,73,78,83,85,86]

mean=[]
error=[]
for mut in mutnum:
    a=[]
    b=[]
    for num in phase:
        a.append(error_mean[mut+num-12])
        b.append((error_std[mut+num-12])**2)
    mean.append(np.mean(a))
    c=np.sqrt(np.sum(b))
    error.append(c/2)
plt.figure(figsize=[8,4])
plt.errorbar([0,1,2,3,4,5,6,7,8,9,10,11,12,13],mean,yerr=error,fmt='s',mec='black',mew=3,color='lightgrey',markersize=15,linewidth=3,ecolor='black',capsize=3,capthick=3)
plt.yticks([0.0,0.2,0.4,0.6,0.8,1.0],weight='bold',fontsize=15,color='black')
plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13],mutnum,weight='bold',fontsize=15,color='black')
plt.ylabel('error rate (%)',weight='bold',fontsize=15,color='black')
plt.xlabel('mutational position',weight='bold',fontsize=15,color='black')
plt.axhline(np.mean(mean),linestyle='dashed',linewidth=3)
sns.despine(trim=True)
plt.savefig('../Figures/HDV14_seq-error.png',bbox_inches='tight',dpi=1000,transparent=True) #saves figure