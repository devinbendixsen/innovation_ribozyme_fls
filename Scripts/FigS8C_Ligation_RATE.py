#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 13 09:39:18 2018

@author: devin
"""

import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import matplotlib as mpl
mpl.rcParams['agg.path.chunksize'] = 10000
font = {'family' : 'sans-serif','weight' : 'bold','size'   : 13}
plt.rc('font', **font)

#%%
ligase=pickle.load(open("../Pickle/normLIGunLIG.p","rb"))
mutnum=pickle.load(open("../Pickle/mutnumHDVLIGLib14.p","rb"))

#%%
rate={}
for seq in ligase:
    rate[seq]=ligase[seq]*0.0012
x=[]
y=[]
for seq in ligase:
    x.append(mutnum[seq])
    y.append(rate[seq])

plt.figure(figsize=[12,4])
plt.yscale('log')
plt.scatter(x,y,s=30,color='blue',edgecolor='black',linewidth=0.5)
plt.axhline(2.4e-10,color='black')
plt.axhline(2.4e-8,linestyle="dashed",color='black')
plt.yticks([1e-9,1e-7,1e-7,1e-5,1e-3,1e-1])
plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14],['HDV',1,2,3,4,5,6,7,8,9,10,11,12,13,'Ligase'])
plt.ylabel('ligation rate $(min^1)$',weight='bold',fontsize=13,color='black')
plt.xlabel('mutational distance',weight='bold',fontsize=13,color='black')
sns.despine(trim=True)
plt.savefig('../Figures/ligation_rate.png',bbox_inches='tight',dpi=1000,transparent=True) #saves figure