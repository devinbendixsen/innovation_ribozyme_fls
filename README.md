# Innovation_Ribozyme_FLS

Bendixsen DP, Collet J, Østman B, Hayden EJ (2019) Genotype network intersections promote evolutionary innovation. PLoS Biol 17(5): e3000300. https://doi.org/10.1371/journal.pbio.3000300 \
This directory is a library that contains data (.xlsx and .p) files and Python scripts that were used in the data analysis of high-througput sequencing data of the intersection between two ribozyme genotype networks (self-cleavage and self-ligation). Raw sequencing data is deposited at the European Nucleotide Archive (ENA) Project No: **PRJEB30802**.


## Installation

The directory is formatted as spyder python project.


## Usage

Python scripts in this directory encompass three categories:\
**(1)** Scripts used to re-create all of the figures in Bendixsen et al. (2019).\
**(2)** Script that calculated the relative activity (fitness) of 16,384 genotypes for self-cleavage and self-ligation from high-throughput sequencing assays (FitnessGenerator_HDV-LIGLib14_1-3v2.py).\
**(3)** Script that simulates evolution on an empirical fitness landscape using a Wright-Fisher model (RiboEvolve.py)
    
Data in this directory encompass two categories:\
**(1)** Excel spreadsheets (.xlsx) that contain either high-throughput sequencing statistics (seq_stat.xlsx) or data generated from Genonets (http://ieu-genonets.uzh.ch/). \
**(2)** Python pickle files (.p) that contain either calculated metrics (Pickle directory) or the results of simulated evolution on empirical RNA fitness landscapes (Results directory).
    
Figures presented in Bendixsen et al. (2019) are found in the Figures directory.

## Authors and acknowledgment

Python scripts were developed by **Devin Bendixsen, PhD** in collaboration with **Eric Hayden, PhD** and **Bjørn Østman, PhD**.